package structs

import "github.com/jinzhu/gorm"

// User data
type User struct {
	gorm.Model
	Username  string `gorm:"type:varchar(191);column:username;" json:"username"`
	Email     string `gorm:"type:varchar(191);column:email;" json:"email"`
	FirstName string `gorm:"type:varchar(191);column:firstName;" json:"firstName"`
	LastName  string `gorm:"type:varchar(191);column:lastName;" json:"lastName"`
	Barcode   string `gorm:"type:text;" json:"barcode"`
	Password  string `json:",omitempty"`
	Token     string `json:"token,omitempty"`
}

// Payment data
type Payment struct {
	gorm.Model
	Username string  `gorm:"type:varchar(191);column:username;" json:"username"`
	Transfer float64 `gorm:"type:decimal(19,2);column:transfer;" json:"transfer"`
}
