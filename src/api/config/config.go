package config

import (
	"github.com/VeirryAu/payment-api/src/api/structs"
	"github.com/jinzhu/gorm"
)

// DBInit create connection to database
func DBInit() *gorm.DB {
	db, err := gorm.Open("mysql", "desata:Desata112244@tcp(192.168.100.14:3306)/db_desata?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic(err.Error())
	}

	db.AutoMigrate(structs.User{})
	db.AutoMigrate(structs.Payment{})
	return db
}
