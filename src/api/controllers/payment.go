package controllers

import (
	"github.com/VeirryAu/payment-api/src/api/apihelpers"
	"github.com/VeirryAu/payment-api/src/api/structs"
	"github.com/gin-gonic/gin"
)

// PaymentRequest to receive req object of json
type PaymentRequest struct {
	Email string
	Total float64
}

// GetUserBalances get total balances of user payment
func (idb *InDB) GetUserBalances(c *gin.Context) {
	var (
		user    structs.User
		payment []structs.Payment
	)
	tokenString := c.Request.Header.Get("Authorization")

	token, tokenValid := ExtractClaims(tokenString)
	if !tokenValid {
		apihelpers.RespondJSON(c, 403, "Unauthorized")
		return
	}
	err := idb.DB.Where("id = ?", token["UserId"]).First(&user).Error
	if err != nil {
		apihelpers.RespondJSON(c, 404, "User not found")
		return
	}
	errBalance := idb.DB.Table("payments").Where("username = ?", user.Username).Find(&payment).RecordNotFound()
	if errBalance {
		apihelpers.RespondJSON(c, 422, "Record not found")
	} else {
		apihelpers.RespondJSON(c, 200, payment)
	}
}

// CreatePayment to the database
func (idb *InDB) CreatePayment(c *gin.Context) {
	var (
		req           PaymentRequest
		paymentSource structs.Payment
		paymentTarget structs.Payment
		source        structs.User
		target        structs.User
	)
	tokenString := c.Request.Header.Get("Authorization")
	token, tokenValid := ExtractClaims(tokenString)
	if !tokenValid {
		apihelpers.RespondJSON(c, 403, "Unauthorized")
		return
	}
	errSource := idb.DB.Where("id = ?", token["UserId"]).First(&source).Error
	if errSource != nil {
		apihelpers.RespondJSON(c, 404, "User not found")
		return
	}
	c.BindJSON(&req)
	errTarget := idb.DB.Where("email = ?", req.Email).First(&target).Error
	if errTarget != nil {
		apihelpers.RespondJSON(c, 404, "User not found")
		return
	}

	if source.ID == target.ID {
		apihelpers.RespondJSON(c, 409, "Cannot send to self account")
	} else {
		paymentSource = structs.Payment{Transfer: req.Total * -1, Username: source.Username}
		paymentTarget = structs.Payment{Transfer: req.Total, Username: target.Username}
		idb.DB.Create(&paymentSource)
		idb.DB.Create(&paymentTarget)
		apihelpers.RespondJSON(c, 200, paymentTarget)
	}
}
