package controllers

import (
	"log"
	"net/http"

	"github.com/VeirryAu/payment-api/src/api/apihelpers"
	"github.com/VeirryAu/payment-api/src/api/structs"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

type Token struct {
	UserId uint
	jwt.StandardClaims
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func ExtractClaims(tokenStr string) (jwt.MapClaims, bool) {
	hmacSecretString := "secret" // Value
	hmacSecret := []byte(hmacSecretString)
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		return hmacSecret, nil
	})

	if err != nil {
		return nil, false
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, true
	} else {
		log.Printf("Invalid JWT Token")
		return nil, false
	}
}

// get all data in user
// func (idb *InDB) GetUsers(c *gin.Context) {
// 	var (
// 		users  []structs.User
// 		result gin.H
// 	)

// 	idb.DB.Find(&users)
// 	if len(users) <= 0 {
// 		result = gin.H{
// 			"result": nil,
// 			"count":  0,
// 		}
// 	} else {
// 		result = gin.H{
// 			"result": users,
// 			"count":  len(users),
// 		}
// 	}

// 	c.JSON(http.StatusOK, result)
// }

// Login to get token and login
func (idb *InDB) Login(c *gin.Context) {
	var (
		body   structs.User
		user   structs.User
		result gin.H
	)
	c.BindJSON(&body)
	idb.DB.Where("username = ?", body.Username).First(&user)
	if (structs.User{}) == user {
		result = gin.H{
			"result": "username not find",
		}
		apihelpers.RespondJSON(c, 404, result)
		return
	}

	isValidPassword := CheckPasswordHash(body.Password, user.Password)
	if isValidPassword {
		user.Password = ""
		tk := &Token{UserId: user.ID}
		sign := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
		token, err := sign.SignedString([]byte("secret"))
		user.Token = token
		if err != nil {
			apihelpers.RespondJSON(c, 500, "Internal server error")
		} else {
			result = gin.H{
				"result": user,
			}
			apihelpers.RespondJSON(c, 200, result)
		}
		return
	}
	result = gin.H{
		"result": "username or password is invalid",
	}
	apihelpers.RespondJSON(c, 403, result)
}

// MyData Get using token
func (idb *InDB) MyData(c *gin.Context) {
	var (
		user structs.User
		// result gin.H
	)
	tokenString := c.Request.Header.Get("Authorization")

	token, tokenValid := ExtractClaims(tokenString)
	if !tokenValid {
		apihelpers.RespondJSON(c, 403, "Unauthorized")
		return
	}
	idb.DB.Where("id = ?", token["UserId"]).First(&user)

	if (structs.User{}) == user {
		apihelpers.RespondJSON(c, 403, "Unauthorized")
		return
	}

	user.Password = ""
	user.Token = tokenString
	apihelpers.RespondJSON(c, 200, user)
}

// CreateUser new data to the database
func (idb *InDB) CreateUser(c *gin.Context) {
	var (
		user structs.User
	)
	c.BindJSON(&user)
	hash, _ := HashPassword(user.Password)
	user.Password = hash
	idb.DB.Create(&user)
	err := idb.DB.NewRecord(user)
	if err {
		apihelpers.RespondJSON(c, 422, "failed to create new user")
	} else {
		user.Password = ""
		apihelpers.RespondJSON(c, 200, user)
	}
}

// UpdateUser data with {id} as query
func (idb *InDB) UpdateUser(c *gin.Context) {
	id := c.Query("id")
	firstName := c.PostForm("firstName")
	lastName := c.PostForm("lastName")
	var (
		user    structs.User
		newUser structs.User
		result  gin.H
	)

	err := idb.DB.First(&user, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	newUser.FirstName = firstName
	newUser.LastName = lastName
	err = idb.DB.Model(&user).Updates(newUser).Error
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "successfully updated data",
		}
	}
	c.JSON(http.StatusOK, result)
}

// // delete data with {id}
// func (idb *InDB) DeleteUser(c *gin.Context) {
// 	var (
// 		user   structs.User
// 		result gin.H
// 	)
// 	id := c.Param("id")
// 	err := idb.DB.First(&user, id).Error
// 	if err != nil {
// 		result = gin.H{
// 			"result": "data not found",
// 		}
// 	}
// 	err = idb.DB.Delete(&user).Error
// 	if err != nil {
// 		result = gin.H{
// 			"result": "delete failed",
// 		}
// 	} else {
// 		result = gin.H{
// 			"result": "Data deleted successfully",
// 		}
// 	}

// 	c.JSON(http.StatusOK, result)
// }
