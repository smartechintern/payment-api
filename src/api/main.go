package main

import (
	"fmt"

	"github.com/VeirryAu/payment-api/src/api/config"
	"github.com/VeirryAu/payment-api/src/api/controllers"
	"github.com/VeirryAu/payment-api/src/api/structs"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

//CORSMiddleware ...
func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Max-Age", "86400")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS, POST, PUT")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding, x-access-token")
		c.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Length")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Content-Type", "application/json")

		if c.Request.Method == "OPTIONS" {
			fmt.Println("OPTIONS")
			c.AbortWithStatus(200)
		} else {
			c.Next()
		}
	}
}

func main() {
	db := config.DBInit()
	inDB := &controllers.InDB{DB: db}

	router := gin.Default()
	router.Use(CORSMiddleware())

	// Websocket
	hub := newHub()
	go hub.run()
	router.GET("/ws", func(c *gin.Context) {
		var (
			user structs.User
		)
		c.Bind(&user)
		serveWs(hub, c.Writer, c.Request)
	})

	// API
	api := router.Group("/api")

	api.POST("/auth/login", inDB.Login)
	api.GET("/auth/me", inDB.MyData)

	api.GET("/users/balances", inDB.GetUserBalances)
	api.POST("/users", inDB.CreateUser)
	api.PUT("/users/:id", inDB.UpdateUser)

	api.POST("/payment/create", inDB.CreatePayment)
	router.Run(":8080")
}
